package com.health.premium.models;

public class Habits {
	
	private boolean isSmoker;
	private boolean isAlcoholic;
	private boolean dailyExcercise;
	private boolean isDruggAddict;
	
	public boolean isSmoker() {
		return isSmoker;
	}

	public void setSmoker(boolean isSmoker) {
		this.isSmoker = isSmoker;
	}

	public boolean isAlcoholic() {
		return isAlcoholic;
	}

	public void setAlcoholic(boolean isAlcoholic) {
		this.isAlcoholic = isAlcoholic;
	}

	public boolean isDailyExcercise() {
		return dailyExcercise;
	}

	public void setDailyExcercise(boolean dailyExcercise) {
		this.dailyExcercise = dailyExcercise;
	}

	public boolean isDruggAddict() {
		return isDruggAddict;
	}

	public void setDruggAddict(boolean isDruggAddict) {
		this.isDruggAddict = isDruggAddict;
	}
	public String toString() {
		return "Habits [isSmoker=" + isSmoker + ", isAlcoholic=" + isAlcoholic
				+ ", dailyExcercise=" + dailyExcercise + ", isDruggAddict="
				+ isDruggAddict + "]";
	}

}
