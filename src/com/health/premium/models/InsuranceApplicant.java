package com.health.premium.models;

public class InsuranceApplicant {
		
	private String name;
	private String gender;
	private int age;
	private Habits habits;
	private HealthStatus healthStatus;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	public HealthStatus getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(HealthStatus healthStatus) {
		this.healthStatus = healthStatus;
	}
	public String toString() {
		return "InsuranceApplicant [name=" + name + ", gender=" + gender
				+ ", age=" + age + ", habits=" + habits + ", healthStatus="
				+ healthStatus + "]";
	}

}
