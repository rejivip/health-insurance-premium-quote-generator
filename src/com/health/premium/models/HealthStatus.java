package com.health.premium.models;

public class HealthStatus {
	
	private boolean isHypertension;
	private boolean isBloodPressure;
	private boolean isBloodSugar;
	private boolean isOverWeight;
	
	public boolean isHypertension() {
		return isHypertension;
	}
	public void setHypertension(boolean isHypertension) {
		this.isHypertension = isHypertension;
	}
	public boolean isBloodPressure() {
		return isBloodPressure;
	}
	public void setBloodPressure(boolean isBloodPressure) {
		this.isBloodPressure = isBloodPressure;
	}
	public boolean isBloodSugar() {
		return isBloodSugar;
	}
	public void setBloodSugar(boolean isBloodSugar) {
		this.isBloodSugar = isBloodSugar;
	}
	public boolean isOverWeight() {
		return isOverWeight;
	}
	public void setOverWeight(boolean isOverWeight) {
		this.isOverWeight = isOverWeight;
	}
	
	public String toString() {
		return "HealthStatus [isHypertension=" + isHypertension
				+ ", isBloodPressure=" + isBloodPressure + ", isBloodSugar="
				+ isBloodSugar + ", isOverWeight=" + isOverWeight + "]";
	}
}
