package com.health.premium;

import com.health.premium.constants.BusinessRuleConstants;
import com.health.premium.models.Habits;
import com.health.premium.models.HealthStatus;
import com.health.premium.models.InsuranceApplicant;

public class HealthInsurancePremium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InsuranceApplicant applicant = new InsuranceApplicant();
		applicant.setAge(34);
		applicant.setGender("MALE");
		HealthStatus status = new HealthStatus();
		status.setHypertension(false);
		status.setBloodPressure(false);
		status.setBloodSugar(false);
		status.setOverWeight(true);
		applicant.setHealthStatus(status);
		Habits habits = new Habits();
		habits.setSmoker(false);
		habits.setAlcoholic(true);
		habits.setDailyExcercise(true);
		habits.setDruggAddict(false);
		applicant.setHabits(habits);
		System.out.println("Insurance premium "+new HealthInsurancePremium().calculateInsurancePremium(applicant));

	}
	
	public int calculateInsurancePremium(InsuranceApplicant applicant){
		int premium = BusinessRuleConstants.BASIC_PREMIUM;
		int age = applicant.getAge();
		String gender = applicant.getGender();
		HealthStatus healthStatus = applicant.getHealthStatus();
		Habits habits = applicant.getHabits();
		if(age < BusinessRuleConstants.AGE_EIGHTEEN){
			return BusinessRuleConstants.BASIC_PREMIUM;
		}else{
			premium = getAgewisePremium(age, premium);
		}
		if(BusinessRuleConstants.GENDER_MALE.equals(gender)){
			premium = premium+(premium*2/100);
		}
		if(healthStatus != null){
			premium = calculatePremiumOverHealthCondition(healthStatus, premium);
		}
		if(habits != null){
			premium = calculaePremiumOverHabits(habits, premium);
		}
		
		return premium;
	}
	
	private int getAgewisePremium(int age, int premium){
		int premium1 = premium + (premium*10/100);
		int premium2 = premium1+(premium1*10/100);
		int premium3 = premium2+(premium2*10/100);
		int premium4 = premium3+(premium3*10/100);
		if(18 < age && age <= 25){
			premium = premium1;
		}else if(25 < age && age <= 30){
			premium = premium2;
		}else if(30 < age && age <= 35){
			premium = premium3;
		}else if(36 < age && age <= 40){
			premium = premium4;
		}else if(age > 40){
			int percentageAboveAgeFourty = getPercentageAboveAgeFourty(age);
			premium = premium + (premium*percentageAboveAgeFourty/100);
		}
		return premium;
	}
	
	private int calculatePremiumOverHealthCondition(HealthStatus healthStatus, int premium){
		if( healthStatus.isBloodPressure()){
			premium = premium+premium*1/100;
		}
		if( healthStatus.isBloodSugar()){
			premium = premium+premium*1/100;
		}
		if( healthStatus.isHypertension()){
			premium = premium+premium*1/100;
		}
		if( healthStatus.isOverWeight()){
			premium = premium+premium*1/100;
		}
		return premium;
	}
	
	private int calculaePremiumOverHabits(Habits habits, int premium){
		if(habits.isAlcoholic()){
			premium = premium+premium*3/100;
		}
		if(habits.isDailyExcercise()){
			premium = premium-premium*3/100;
		}
		if(habits.isDruggAddict()){
			premium = premium+premium*3/100;
		}
		if(habits.isSmoker()){
			premium = premium+premium*3/100;
		}
		return premium;
	}
	
	private int getPercentageAboveAgeFourty(int age){
		int percentageDiff = 20;
		int midAge = 40;
		int midPercentage = 60;
		int ageDiff = 0;
		int actualAgeGroupLowerValue = age-(age%5);
		ageDiff = actualAgeGroupLowerValue-midAge;
		int actualPercentage = midPercentage+(((ageDiff/5)-1)*percentageDiff);
		
		return actualPercentage;
	}

}
