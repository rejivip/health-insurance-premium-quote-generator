package com.health.premium.constants;

public class BusinessRuleConstants {
	
	public static final int AGE_EIGHTEEN = 18;
	public static final int AGE_TWENTY_FIVE = 25;
	public static final int AGE_THIRTY = 30;
	public static final int AGE_THIRTY_FIVE = 35;
	public static final int AGE_FOURTY = 40;
	
	public static final int PERCENTAGE_TEN = 10;
	public static final int PERCENTAGE_TWENTY = 20;
	public static final int PERCENTAGE_ONE = 1;
	public static final int PERCENTAGE_TWO = 2;
	public static final int PERCENTAGE_THREE = 3;
	
	public static final int BASIC_PREMIUM = 5000;
	
	public static final String GENDER_MALE = "MALE";
	public static final String GENDER_FEMALE = "FEMALE";
	


}
